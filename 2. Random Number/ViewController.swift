//
//  ViewController.swift
//  2. Random Number
//
//  Created by x0000ff on 11/07/15.
//  Copyright (c) 2015 x0000ff. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var randomNumberLabel: UILabel!

    @IBAction func generatedRandomNumberTapped(sender: AnyObject) {
   
        // Максимальное число
        let maxNumber = 100
        
        // Генерируем случайное число
        let randomNumber = arc4random() % UInt32(maxNumber)
        
        // Выводим сгенерированное число в консоль отладки ( Debug console )
        println(randomNumber)
        
        // Выводим сгенерированное число на экран,
        // присваивая его значению "text" нашей метки "randomNumberLabel"
        randomNumberLabel.text = String(randomNumber)
        
        // String(randomNumber) - необходимо это приведение / конвертирование числа в строку
    }
    
}

